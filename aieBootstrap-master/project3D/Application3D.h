#pragma once

#include "Application.h"
#include <glm/mat4x4.hpp>
#include "Gizmos.h"
#include "Input.h"
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm\gtc\matrix_transform.hpp>
using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

class BaseCamera;

class Application3D : public aie::Application {
private:
	struct Vertex {
		vec4 position;
		vec4 colour;
	};
public:

	Application3D();
	virtual ~Application3D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();


	void generateGrid(unsigned int rows, unsigned int cols);

protected:


	BaseCamera* m_camera;

	void setupShader();
	unsigned int m_programID;

	//our vertex and index buffers
	unsigned int m_VAO;
	unsigned int m_VBO;
	unsigned int m_IBO;
	unsigned int m_numberofIndices;

	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;
	glm::mat4   m_orthographicMatrix;
};