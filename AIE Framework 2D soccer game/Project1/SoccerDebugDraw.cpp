#include "SoccerDebugDraw.h"
#include "SpriteBatch.h"

SoccerDebugDraw* SoccerDebugDraw::sm_instance = nullptr;

SoccerDebugDraw::SoccerDebugDraw()
{
}


SoccerDebugDraw::~SoccerDebugDraw()
{
}

SoccerDebugDraw * SoccerDebugDraw::GetInstance()
{
	if (sm_instance == nullptr)
		sm_instance = new SoccerDebugDraw();


	return sm_instance;

}

void SoccerDebugDraw::DestroySingleton()
{
	sm_instance->Clear();
	delete sm_instance;
	sm_instance = nullptr;
}

void SoccerDebugDraw::Clear()
{
	for (int i = 0; i < m_drawList.size(); i++)
	{
		delete m_drawList[i];
	}

	m_drawList.clear();
}

void SoccerDebugDraw::Draw(SpriteBatch * pSpriteBatch)
{
	for (int i = 0; i < m_drawList.size(); i++)
	{
		switch (m_drawList[i]->eType)
		{
		case POINT:
			DrawPoint((PointDrawInfo*)m_drawList[i], pSpriteBatch);
			break;
		default:
			break;
		}
	}
}

void SoccerDebugDraw::AddDrawPoint(const Vector2 & Position, unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
	PointDrawInfo* pInfo = new PointDrawInfo();
	pInfo->eType = POINT;
	pInfo->Position = Position;
	pInfo->r = r;
	pInfo->g = g;
	pInfo->b = b;
	pInfo->a = a;

	m_drawList.push_back(pInfo);
}

void SoccerDebugDraw::DrawPoint(PointDrawInfo * pPointInfo, SpriteBatch * pSpriteBatch)
{
	pSpriteBatch->SetRenderColor(pPointInfo->r, pPointInfo->g, pPointInfo->b, pPointInfo->a);

	pSpriteBatch->DrawLine(pPointInfo->Position.x - 5.0f,
		pPointInfo->Position.y - 5.0f,
		pPointInfo->Position.x + 5.0f,
		pPointInfo->Position.y + 5.0f, 2.0f);

	pSpriteBatch->DrawLine(pPointInfo->Position.x - 5.0f,
		pPointInfo->Position.y + 5.0f,
		pPointInfo->Position.x + 5.0f,
		pPointInfo->Position.y - 5.0f, 2.0f);
}
