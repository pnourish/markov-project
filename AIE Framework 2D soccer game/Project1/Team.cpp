#include "Team.h"
#include "Goal.h"
#include "ball.h"


Team::Team()
{
	m_teamsGoal = nullptr;
	m_oppositionsGoal = nullptr;
	m_gameBall = nullptr;
}

Team::Team(Goal* a_teamGoal, Goal* a_oppositionGoal, Ball* a_gameBall)
{
	m_teamsGoal = a_teamGoal;
	m_oppositionsGoal = a_oppositionGoal;
	m_gameBall = a_gameBall;
}

Team::~Team()
{
}

void Team::AddMember(Character* a_memberToAdd)
{
	teamMembers.push_back(a_memberToAdd);
}

Character* Team::GetTeamMember(Character::eFIELDPOSITION a_pos)
{
	for (int i = 0; i < teamMembers.size(); i++)
	{
		if (teamMembers[i]->m_fieldPosition == a_pos)
		{
			return teamMembers[i];
		}
		
	}
	return nullptr;
}

Character* Team::GetTeamMembers(int a_teamMemberChoice)
{
	return teamMembers[a_teamMemberChoice];
}

std::vector<Character*>* Team::GetTeamMembersArray()
{
	return &teamMembers;
}

bool Team::getBallContact()
{
	return m_ballContact;
}

void Team::setBallContactOpposite()
{
	m_ballContact = !m_ballContact;
}

void Team::SetGoal(Goal* a_goalChoice)
{
	m_teamsGoal = a_goalChoice;
}

Goal* Team::GetGoal()
{
	return m_teamsGoal;
}

void Team::SetBall(Ball* a_ballChoice)
{
	m_gameBall = a_ballChoice;
}

Ball* Team::GetBall()
{
	return m_gameBall;
}

void Team::SetOppositionGoal(Goal* a_goalChoice)
{
	m_oppositionsGoal = a_goalChoice;
}

Goal* Team::GetOppositionGoal()
{
	return m_oppositionsGoal;
}

void Team::SetTeamDirection(int a_directionChoice)
{
	m_teamDirection = a_directionChoice;
}

int Team::GetTeamDirection()
{
	return m_teamDirection;
}

void Team::SetOppositionTeam(Team* a_oppositionTeam)
{
	m_oppositionTeam = a_oppositionTeam;
}

Team* Team::GetOppositionTeam()
{
	return m_oppositionTeam;
}
