#include "Opposition.h"



Opposition::Opposition()
{
}

Opposition::Opposition(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice)
{
	m_entityTexture = a_texture;
	position.x = a_pos.x;
	position.y = a_pos.y;
	SetTeam(a_teamChoice);
}
Opposition::~Opposition()
{
}

void Opposition::Update(float deltaTime)
{
	std::cout << (int)currState << std::endl;	//Opposition boundary collisions
	if (position.x - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(0 + m_entityTexture->GetWidth() / 2.0f, position.y);
	}
	if (position.x + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.x)
	{
		position = Vector2(m_windowBoundaries.x - m_entityTexture->GetWidth() / 2.0f, position.y);
	}
	if (position.y - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(position.x, 0 + m_entityTexture->GetWidth() / 2.0f);
	}
	if (position.y + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.y)
	{
		position = Vector2(position.x, m_windowBoundaries.y - m_entityTexture->GetWidth() / 2.0f);
	}
	position = position + GetVelocity() * deltaTime;
	Vector2 zero;
	SetVelocity(GetVelocity() * 0.9f);
}


void Opposition::SetWindowBoundaries(Vector2 a_boundaries)
{
	m_windowBoundaries = a_boundaries;
}

