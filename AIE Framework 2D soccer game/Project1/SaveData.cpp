#include "SaveData.h"

std::map<std::string, int> SaveData::savedInts;

void SaveData::SetInt(std::string a_name, int a_value)
{
	savedInts[a_name] = a_value;
}

int SaveData::GetInt(std::string a_name)
{
	if (savedInts.find(a_name) != savedInts.end())
	{
		return savedInts[a_name];
	}
}
