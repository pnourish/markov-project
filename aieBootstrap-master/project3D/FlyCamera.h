#pragma once
#include "BaseCamera.h"

struct GLFWwindow;

class FlyCamera :
	public BaseCamera
{
public:
	FlyCamera(GLFWwindow* window, float speed);
	~FlyCamera() = default;


	void Update(float deltaTime) override;
	void SetSpeed(float speed);
	float GetSpeed() { return m_speed; };
	void BasicRotation();

private:

	GLFWwindow* m_window;
	float m_speed;
	glm::vec3 up;
};

