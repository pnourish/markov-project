#pragma once
#include "CollisionManager.h"
#include "Player.h"
#include "Ball.h"
#include "AIAgent.h"
class KeepingsOffScene
{
public:
	KeepingsOffScene();
	~KeepingsOffScene();
	void Startup();//allocate all resources, create ageents, 
											 //create player, create everything
	void Update(float deltaTime);
	void DeltaTimeUpdate(float deltaTime);

	void Draw(SpriteBatch* m_spritebatch);

	void Shutdown(); //destroy everything



	void DrawVector(Vector2 origin, Vector2 dir);
	void DrawPoint(Vector2 origin);
	//void ScoreRestart();
	void ResetScores();
	static void AddScore(int teamid);
	static Vector2 vecFromGoal;
	static Vector2 SeekPos;
	CollisionManager collisionManager;


	int windowWidth = 1232;
	int windowHeight = 878;

	bool hasGameEnded = false;

	//vector of entities
	//textures (maybe application holds textures


protected:
	SpriteBatch *m_spritebatch;
private:
	Texture *m_backgroundTexture;
	Texture *m_pAgentTexture;
	Texture *m_soccerBallTextture;
	Texture *m_playerTexture;
	Texture *m_playerTeamTexture;
	Texture *m_oppositionTexture;
	float m_gameTimer = 30;
	float m_roundLength = 30;
	static int PlayerScore;
	static int oppositionScore;
	static float disableTimer;
	static float disableLength;
	Font *m_font;
	int m_mouseX;
	int m_mouseY;
	Player* m_player;
	Ball* m_ball;
	DynamicArrayTemplate<AIAgent*> oppositionArray;
	int numberofenemies = 3;
	DynamicArrayTemplate<AIAgent*> teamMateArray;
	int numberofteammates = 2;
	Input* m_input;
	float m_DeltaTime;
	Vector2 startValue;
	Vector2 endValue;
	Vector2 noVelocity;
	Team team1;
	Team team2;
};

