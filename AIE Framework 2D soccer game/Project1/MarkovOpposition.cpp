#include "MarkovOpposition.h"
#include "Game1.h"



MarkovOpposition::MarkovOpposition(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice)
	: AIAgent(a_texture, a_pos, a_teamChoice)
{
	currState = eAIState::BASICSEEK;
	m_fieldPosition = Character::eFIELDPOSITION::MARKOVOPPOSITION;
	moveSpeed = (float)midfielderSpeed;
	oldestPlayer = 100;
	middlePlayer = 100;
	currentPlayer = 100;
	predictedNextPlayer = 100;
	opponentTouchedBall = false;
	midPointReached = false;

}


MarkovOpposition::~MarkovOpposition()
{
}

void MarkovOpposition::Update(float deltaTime, std::map<int, Vector2> playerPositionArray)
{
	//work out line vector from AI towards the ball
	Vector2 vecBetweenOppositionBall = position - m_team->GetBall()->position;
	
	switch (currState)
	{
	default:
	case eAIState::SEEK:
		if(chaseBall == true)
		BasicSeek(m_team->GetBall()->position, this);
		else if (chaseMiddle == true)
		{
			nextPosition = playerPositionArray[predictedNextPlayer];
			interceptPosition = AIFindMidPoint(playerPositionArray[currentPlayer], nextPosition);
	
			BasicSeek(interceptPosition, this);
		}
	}
	
	AIAgent::Update(deltaTime);
}

void MarkovOpposition::OnNotify(int EventID, void * data)
{
	if (EventID == EVENT_TOUCHED)
	{


		oldestPlayer = middlePlayer;
		middlePlayer = currentPlayer;
		int playerID = *(int*)data;
		//get back to the map from before
		currentPlayer = playerID;

		std::cout << "Ball just left: " << currentPlayer << std::endl;

		markovDecisionMaker.Add(oldestPlayer, middlePlayer, currentPlayer);
		predictedNextPlayer = markovDecisionMaker.GetNext(middlePlayer, currentPlayer);
		std::cout << "Ball should go to: " << predictedNextPlayer << std::endl;
		chaseBall = false;
		chaseMiddle = true;

		
	}
	if (EventID == EVENT_RELEASED)
	{
		chaseBall = true;
		chaseMiddle = false;
	}
}
