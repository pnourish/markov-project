#include "MainMenuScene.h"



MainMenuScene::MainMenuScene()
{


}


MainMenuScene::~MainMenuScene()
{

}

void MainMenuScene::Startup()
{	

	m_font = new Font("./Resources/Fonts/calibri_36px.fnt");

	std::string standardGameSize = "  Standard Game";
	float wordSize = 16 * 14;
	standardGame = new Button( Vector2((windowWidth * 0.5) - (wordSize/2), windowHeight * 0.2),  45,  wordSize,  m_font);
	standardGame->setMessage(standardGameSize.c_str());

	std::string KeepingsOffSize = "     Keepings Off";
	wordSize = 16 * 14;
	KeepingsOff = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.3), 45, wordSize, m_font);
	KeepingsOff->setMessage(KeepingsOffSize.c_str());

	std::string MarkovSize = "          Markov";
	wordSize = 16 * 14;
	Markov = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.4), 45, wordSize, m_font);
	Markov->setMessage(MarkovSize.c_str());

	std::string QuitSize = "            Quit";
	wordSize = 16 * 14;
	Quit = new Button(Vector2((windowWidth * 0.5) - (wordSize / 2), windowHeight * 0.5), 45, wordSize, m_font);
	Quit->setMessage(QuitSize.c_str());
}

void MainMenuScene::Update()
{
}

void MainMenuScene::Draw(SpriteBatch* m_spritebatch)
{

	standardGame->Draw(m_spritebatch);	
	KeepingsOff->Draw(m_spritebatch);
	Markov->Draw(m_spritebatch);
	Quit->Draw(m_spritebatch);

}

void MainMenuScene::Shutdown()
{
	delete m_font;

	delete standardGame;
	delete KeepingsOff;
	delete Markov;
	delete Quit;
}
