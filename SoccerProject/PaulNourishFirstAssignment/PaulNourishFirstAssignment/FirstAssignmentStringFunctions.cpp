#include <iostream>


int MyStringLength(char* str)
{
	
	int i = 0;
	while (str[i] != 0)
	{
		i++;
	}
	return i;
}
char MyStringCharAt(char* str1, int a_index)
{
	
	//Check if a_index is less than the total length of the string
	if (a_index < MyStringLength(str1))
	{
		// get the value of the char at the a_index position
		
		//return Value
		return str1[ a_index ];
	}
	else
	{
		return 0;
	}


}
int MyStringCompare(char* str1, char* str2)
{
	int character_index = 0;
	int result = 0;
	bool running = true;
	while (running = true)
	{
		if (str1[character_index] == 0 && str2[character_index] == 0)
		{
			result = 0;
			break;
		}
		else if (str1[character_index] > str2[character_index])
		{
			result = 1;
			break;
		}
		else if (str1[character_index] < str2[character_index])
		{
			result = -1;
			break;
		}
		else
		{
			++character_index;
		}
						
			
	}

	return result;
}
void MyStringAppend (char* str1, char* str2)
{
	int destination_index = MyStringLength(str1);

	int source_index = 0;

	while (str2[source_index] != 0)
	{
		str1[destination_index] = str2[source_index];
		++destination_index;
		++source_index;
	}
	str1[destination_index] = 0;
	
	

}
void MyCopyFunction(char* str1, char* str2)
{
	for (int i = 0; i < MyStringLength(str2) +1; i++)
	{
		str1[i] = str2[i];
	}
}
void MyStringPrepend (char* str1, char* str2)
{
	char str3[100] = { 0 };
	MyCopyFunction(str3, str2);
	MyStringAppend(str3, str1);
	MyCopyFunction(str1, str3);

}
void MyStringToLower(char* str1, char* output)
{
	char* str2 = { 0 };
	MyCopyFunction(str2, str1);
	for (int i = 0; i < MyStringLength(str2); i++)
	{
		if (str2[i] >= 65 && str2[i] <= 90)
		str2[i] += 32;
	
	}
	MyCopyFunction(output, str2);
}
void MyStringToUpper(char* str1, char* output)
{
	char* str2 = { 0 };
	MyCopyFunction(str2, str1);
	for (int i = 0; i < MyStringLength(str2); i++)
	{
		if (str2[i] >= 97 && str2[i] <= 122)
			str2[i] -= 32;

	}
	MyCopyFunction(output, str2);
}
int MyFindSubString(char* str1, char* substr1)

{
	int j = 0;
	for (int i = 0; i < MyStringLength(str1); i++)
	{
		while (j < MyStringLength(substr1))
		{
			if (i == MyStringLength(str1)-1 && str1[i] != substr1[j])
			{
				return 0;
			}
			else if (str1[i] == substr1[j])
			{
				 
				if (MyStringLength(substr1) == j+1)
				{
					return i - MyStringLength(substr1) +2;
				}
				else
				{
					j++;
					break;
				}		
			}
	/*		else if (MyStringLength(substr1) == j)
			{
				return i;
			}*/  
			else if (str1[i] != substr1[j])
			{
				j = 0;
				break;
			}
		
		}
	}
}
int MyDelayedFindSubString(int startIndex, char* str1, char* substr1)
{
	int j = 0;
	for (int i = startIndex; i < MyStringLength(str1); i++)
	{
		while (j < MyStringLength(substr1))
		{
			if (i == MyStringLength(str1) - 1 && str1[i] != substr1[j])
			{
				return 0;
			}
			else if (str1[i] == substr1[j])
			{

				if (MyStringLength(substr1) == j + 1)
				{
					return i - MyStringLength(substr1) + 2;
				}
				else
				{
					j++;
					break;
				}
			}
			/*		else if (MyStringLength(substr1) == j)
			{
			return i;
			}*/
			else if (str1[i] != substr1[j])
			{
				j = 0;
				break;
			}

		}
	}
}



int main()

{
	//Length
	char test1[100] = "is a beautiful bastard! ";
	char test2[100] = "PAUL NOURISH ";
	int findPosition = 18;
	int result = MyStringLength(test1);
	char* lowerOutput = { 0 };
	char* upperOutput = { 0 };
	char string[100] = "Paul Nourish";
	char substring[100] = "Pa";


//	MyStringLength(test1);
//	{
//		std::cout << "There are " << result << " characters in this string." << std::endl;
//	}
//
//	//Charat
//	char result1 = MyStringCharAt(test1, findPosition);
//	{
//		std::cout << "The character at " << findPosition << " is " <<  result1 << std::endl;
//	}
//	//Compare
//	int result2 = MyStringCompare(test1, test2);
//	{
//		std::cout << "The value is " << result2 << std::endl;
//	}
//	//Append

	MyStringAppend(test1, test2);
	std::cout << "The new string is " << test1 << std::endl;
	MyCopyFunction(test1, test2);
	std::cout << "The new string is" << test1 << std::endl;
	
//	//Prepend
//
//		MyStringPrepend(test1, test2);
//	{
//		std::cout << "The new string is: " << test1 << std::endl;
//	}
//
//	//Tolower
//
//	MyStringToLower(test2, lowerOutput);
//	{
//		std::cout << "This String is in lower case " << lowerOutput << std::endl;
//	}
//
//	//ToUpper
//
//	MyStringToUpper(test1, upperOutput);
//	{
//		std::cout << "This String is in Upper case " << upperOutput << std::endl;
//	}
//	
//	//FindSubString
//
//	int subStringResult = MyFindSubString(string, substring);
//	{
//		if (subStringResult == 0)
//		{
//			std::cout << "The string was not found" << std::endl;
//		}
//		else 
//		{
//			std::cout << "the character at position " << subStringResult  << " was the first character of your substring" << std::endl;
//		}
//	}
//
//
//	//delayedFindSubString
//
//	int delayedSubStringResult = MyDelayedFindSubString(3, string, substring);
//	{
//		if (delayedSubStringResult == 0)
//		{
//			std::cout << "The string was not found" << std::endl;
//		}
//		else
//		{
//			std::cout << "the character at position " << delayedSubStringResult << " was the first character of your substring" << std::endl;
//		}
//	}

	system("pause");
	
}