#include "Ball.h"



Ball::Ball()
{
}

Ball::Ball(Texture* a_texture, Vector2 a_pos)
{
	m_entityTexture = a_texture;
	position.x = a_pos.x;
	position.y = a_pos.y;
}

Ball::~Ball()
{
}


void Ball::Update(float deltaTime)
{
	if (position.x - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(0 + m_entityTexture->GetWidth() / 2.0f, position.y);
		m_velocity.x *= -1.0f;
	}
	if (position.x + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.x)
	{
		position = Vector2(m_windowBoundaries.x - m_entityTexture->GetWidth() / 2.0f, position.y);
		Vector2 velocity = GetVelocity();
		velocity.x *= -1.0f;
		SetVelocity(velocity);
	}
	if (position.y - m_entityTexture->GetWidth() / 2.0f <= 0.0f)
	{
		position = Vector2(position.x, 0 + m_entityTexture->GetWidth() / 2.0f);
		Vector2 velocity = GetVelocity();
		velocity.y *= -1.0f;
		SetVelocity(velocity);
	}
	if (position.y + m_entityTexture->GetWidth() / 2.0f >= m_windowBoundaries.y)
	{	
		if (m_velocity.y == m_windowBoundaries.y)
		{
			m_velocity.y = m_velocity.y + 50.0f;
		}
		position = Vector2(position.x, m_windowBoundaries.y - m_entityTexture->GetWidth() / 2.0f);
		Vector2 velocity = GetVelocity();
		velocity.y *= -1.0f;
		SetVelocity(velocity);
	}

	position = position + GetVelocity() * deltaTime;
}

void Ball::SetWindowBoundaries(Vector2 a_boundaries)
{
	m_windowBoundaries = a_boundaries;
}

void Ball::OnKick(float deltaTime, int playerID)
{
	if (recentlyTouched == true )
	{
		//LINE BELOW TELLS ALL OBSERVERS THAT THE BALL HAS BEEN RECENTLY TOUCHED
		Notify(EVENT_TOUCHED, (void*)&playerID);
		recentlyTouchedTimer -= deltaTime;
		if (recentlyTouchedTimer <= 0)
		{
			recentlyTouchedTimer = recentlyTouchedFullTime;
			recentlyTouched = false;
			
		}
	}
	if (recentlyTouched == false)
	{
		Notify(EVENT_RELEASED, (void*)&playerID);
	}
}

