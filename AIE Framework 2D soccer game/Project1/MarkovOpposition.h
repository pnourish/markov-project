#pragma once

#include "AIAgent.h"
#include "iObserver.h"
#include "markov.h"



class MarkovOpposition : public AIAgent, public IObserver
{
public:
	MarkovOpposition(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);
	~MarkovOpposition();

	void Update(float deltaTime, std::map<int, Vector2> playerPositionArray);

private:
	bool opponentTouchedBall;
	bool midPointReached;
	int oldestPlayer;
	int middlePlayer;
	int currentPlayer;
	int predictedNextPlayer;
	Vector2 nextPosition;
	Vector2 interceptPosition;

	bool chaseBall;
	bool chaseMiddle;

	markov<int> markovDecisionMaker;

	// Inherited via IObserver
	virtual void OnNotify(int EventID, void * data) override;
};

