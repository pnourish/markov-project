#pragma once
#include "Character.h"
#include "Team.h"

enum class eAIState
{
	SEEK,
	KICK,
	DEFEND,
	ATTACK, 
	MURDER,
	BASICSEEK
};



class AIAgent : public Character
{
public: 
	AIAgent();

	AIAgent(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice);

	~AIAgent();


	virtual void Update(float deltaTime);
	void SetWindowBoundaries(Vector2 a_boundaries);
	Vector2 m_windowBoundaries;


	float originalAvoidSpeed = 10;
	float& avoidSpeed = originalAvoidSpeed;
	float m_kickTime = 0.5f;
	float m_currKickTimer = m_kickTime;
	Vector2 kickDirection;
	bool kickstart;
	bool movedown = false;
	bool moveup = false;
	float m_seekPosOffset = 150;
	
	
	Vector2 SeekPos;
	
	float murderBotSpeed = 20;
	int defenderSpeed = 40;
	int midfielderSpeed = 40;
	int attackerSpeed = 60;


	float deflectionDistance = 50;

	eAIState currState = eAIState::SEEK;

	float Distance(Vector2 obj1, Vector2 obj2);
	void Respawn();

	void BasicSeek(Vector2 desiredPos, AIAgent* a_ai);
	void AIKick(float deltaTime, AIAgent* a_opposition, Ball* a_ball);
	void AISeek(Ball* a_ball, Goal* a_oppositionGoal, AIAgent* a_ai, float a_windowWidth);
	void AIDefend(Ball* a_ball, Goal*a_playerGoal, AIAgent* a_ai, float a_windowWidth);
	void AIHangBackDefence(Goal* a_playerGoal, AIAgent* a_ai, Ball* a_ball);
	void AIDeflection(Goal* a_oppositionGoal, AIAgent* a_ai, Ball* a_gameBall);
	void AIBullyDefender(AIAgent* a_thisAI, Character* a_victim);
	void AIFindSpace(AIAgent* a_thisAI, Character* a_defender);
	void AIPatrol(AIAgent* a_thisAI, float a_windowboundariesx, float a_windowBoundariesy);
	void AIAvoid(AIAgent* a_thisAI, Character* a_murderBot);
	void AIFlee(Character* a_murderBot, float a_lookAheadTime = 1);
	Vector2 AIFindMidPoint(Vector2 m_pos1, Vector2 m_pos2);

};

