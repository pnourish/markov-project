#include "MarkovScene.h"
#include "SpriteBatch.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "glfw3.h"
#include "Application.h"
#include "Vector2.h"
#include <iostream>
#include <amp.h>
#include "amp.h"
#include <ctime>
#include "AIAgent.h"
#include "Midfielder.h"
#include "DynamicArrayTemplate.h"
#include "team.h"
#include "MarkovOpposition.h"

#include "SoccerDebugDraw.h"
#include "SaveData.h"

int MarkovScene::PlayerScore;
int MarkovScene::oppositionScore;
float MarkovScene::disableTimer = 4;
float MarkovScene::disableLength = 4;
Vector2 MarkovScene::vecFromGoal;
Vector2 MarkovScene::SeekPos;

MarkovScene::MarkovScene()
{
}


MarkovScene::~MarkovScene()
{
}

void MarkovScene::Startup()
{

	m_input = Input::GetSingleton();
	// Load in the textures that we want to use
	m_backgroundTexture = new Texture("./Resources/Images/background.png");
	m_pAgentTexture = new Texture("./Resources/Images/Agent.png");
	m_soccerBallTextture = new Texture("./Resources/Images/soccerballblackbackground1.png");
	m_playerTexture = new Texture("./Resources/Images/Player Blue Colour.png");
	m_playerTeamTexture = new Texture("./Resources/Images/Blue Colour.png");
	m_oppositionTexture = new Texture("./Resources/Images/Red Colour.png");

	Vector2 ballPos;
	Vector2 oppositionPos[1];
	Vector2 teamMatePos[9];
	Vector2 murderBotPos[2];

	ballPos.x = (windowWidth / 2);
	ballPos.y = (windowHeight / 2);


	oppositionPos[0].x = windowWidth  * 0.7f;
	oppositionPos[0].y = ((windowHeight / 2)*0.5);

	teamMatePos[0].x = windowWidth  * 0.1f;
	teamMatePos[0].y = ((windowHeight / 2));
	teamMatePos[1].x = windowWidth  * 0.1f;
	teamMatePos[1].y = ((windowHeight / 2)*0.5);
	teamMatePos[2].x = windowWidth  * 0.1f;
	teamMatePos[2].y = ((windowHeight / 2)* 1.5);
	teamMatePos[3].x = windowWidth  * 0.5f;
	teamMatePos[3].y = ((windowHeight / 2));
	teamMatePos[4].x = windowWidth  * 0.5f;
	teamMatePos[4].y = ((windowHeight / 2)*0.5);
	teamMatePos[5].x = windowWidth  * 0.5f;
	teamMatePos[5].y = ((windowHeight / 2)* 1.5);
	teamMatePos[6].x = windowWidth  * 0.9f;
	teamMatePos[6].y = ((windowHeight / 2));
	teamMatePos[7].x = windowWidth  * 0.9f;
	teamMatePos[7].y = ((windowHeight / 2)*0.5);
	teamMatePos[8].x = windowWidth  * 0.9f;
	teamMatePos[8].y = ((windowHeight / 2)* 1.5);

	//creates player, ball and opposition and specifies their textures and starting positions
	m_ball = new Ball(m_soccerBallTextture, ballPos);

	team1.SetBall(m_ball);
	team2.SetBall(m_ball);



	team1.SetTeamDirection(1);
	team2.SetTeamDirection(-1);

	team1.SetOppositionTeam(&team2);
	team2.SetOppositionTeam(&team1);


	oppositionArray.Add(new MarkovOpposition(m_oppositionTexture, oppositionPos[0], &team2));

	//here we add observers to the ball so that they can make decisions based on it
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		MarkovOpposition* pOpp = dynamic_cast<MarkovOpposition*>(oppositionArray[i]);
		m_ball->AddObserver((IObserver*)pOpp);
	}

	for (int i = 0; i < numberofenemies; i++)
	{
		oppositionArray[i]->SetSpawn(oppositionPos[i]);
	}
	team2.AddMember(oppositionArray[0]);

	for (int i = 0; i < numberofenemies; i++)
	{
		team2.AddMember(oppositionArray[i]);
	}

	for (int i = 0; i < numberofteammates; i++)
	{
		teamMateArray.Add(new MarkovPlayer(*m_ball, m_playerTeamTexture, teamMatePos[i], &team1));
	}


	for (int i = 0; i < numberofteammates; i++)
	{
		teamMateArray[i]->SetSpawn(teamMatePos[i]);
		std::cout << "SPAWN x position = " << teamMateArray[i]->GetSpawn().x << " y position = " << teamMateArray[i]->GetSpawn().y << std::endl;
		std::cout << "NOT SPAWN x position = " << teamMateArray[i]->position.x << " y position = " << teamMateArray[i]->position.y << std::endl;
	}

	//adding the various player positions into a map
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		playerPositionArray[i] = teamMateArray[i]->GetSpawn();
		teamMateArray[i]->m_playerID = i;
	}





	m_ball->SetSpawn(ballPos);

	// Create the font for use with draw string
	m_font = new Font("./Resources/Fonts/calibri_36px.fnt");

	//informing all entities of the boundaries of the game
	m_ball->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		oppositionArray[i]->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	}
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		teamMateArray[i]->SetWindowBoundaries(Vector2(windowWidth, windowHeight));
	}

	m_mouseX = 0;
	m_mouseY = 0;

}




void MarkovScene::Update(float deltaTime)
{
	SoccerDebugDraw::GetInstance()->Clear();

	static bool deltaTimeChecked = false;
	if (deltaTimeChecked == false)
	{
		deltaTime = 0.0f;
		deltaTimeChecked = true;
	}
	m_DeltaTime = deltaTime;
	bool gameIsRunning = true;
	m_input->GetMouseXY(&m_mouseX, &m_mouseY);
	disableTimer -= deltaTime;
	if (disableTimer <= 1)
	{
		DeltaTimeUpdate(deltaTime);
	}


}


void MarkovScene::DeltaTimeUpdate(float deltaTime)
{
	m_gameTimer -= deltaTime;
	m_ball->Update(deltaTime);
	if (m_ball->position.x != 0 || m_ball->position.y != 0)
	{
		m_ball->Drag();
	}
	//various collision function calls
	//opposition and ball callision

	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
			if (collisionManager.MarkovPlayerAndBallCollision(teamMateArray[i], m_ball) == true)
			{
				//PlayerScore++;
			}
	}

	int mapSize = 1000;


	//opposition colliding with team mates
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
		{
			for (int j = 0; j < oppositionArray.Size(); j++)
			{
				if (oppositionArray[j]->IsAlive() == true)
				{
					//collisionManager.AiAndAiCollision(oppositionArray[j], teamMateArray[i]);
				}
			}
		}
	}


	//calls update function on all opposition AIs
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (oppositionArray[i]->IsAlive() == true)
			((MarkovOpposition*)oppositionArray[i])->Update(deltaTime, playerPositionArray);
	}

	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
		{
			//	teamMateArray[i]->Update(deltaTime);
		}
	}


	//restores game to starting settings if game timer runs out
	if (m_gameTimer <= 1)
	{
		hasGameEnded = true;
	}
	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
		{
			teamMateArray[i]->Update(deltaTime, m_input);
		}
	}
	//m_player->Update(deltaTime, m_input);
}

void MarkovScene::Draw(SpriteBatch* m_spritebatch)
{

	// clear the back buffer


	m_spritebatch->SetRenderColor(80, 80, 80, 255);
	m_spritebatch->DrawLine((windowWidth / 2), 0 - 100, (windowWidth / 2), windowHeight + 100, 5);
	m_spritebatch->SetRenderColor(255, 255, 255, 255);
	//draw entity



	for (int i = 0; i < teamMateArray.Size(); i++)
	{
		if (teamMateArray[i]->IsAlive() == true)
			teamMateArray[i]->Draw(m_spritebatch);
	}

	m_ball->Draw(m_spritebatch);
	for (int i = 0; i < oppositionArray.Size(); i++)
	{
		if (oppositionArray[i]->IsAlive() == true)
			oppositionArray[i]->Draw(m_spritebatch);
	}
	//this line resets the render colour to white
	m_spritebatch->SetRenderColor(255, 255, 255, 255);

	//these three lines draw the deltatime in the top left corner
	char buffer[100];
	itoa(m_DeltaTime * 1000.0f, buffer, 10);
	m_spritebatch->DrawString(m_font, buffer, 10, 10);



	float gameTimerLoc = 0.0f;
	gameTimerLoc = (windowWidth * 0.48);
	m_spritebatch->SetRenderColor(255, 255, 255, 255);
	char buffer4[10];
	itoa(m_gameTimer, buffer4, 10);
	m_spritebatch->DrawString(m_font, buffer4, gameTimerLoc, 10);

	if (disableTimer >= 1.0f)
	{
		float disableTimerLoc = 0.0f;
		disableTimerLoc = (windowWidth * 0.48);
		m_spritebatch->SetRenderColor(255, 255, 255, 255);
		char buffer5[10];
		itoa(disableTimer, buffer5, 10);
		m_spritebatch->DrawString(m_font, buffer5, gameTimerLoc, 50);
	}



	SoccerDebugDraw::GetInstance()->Draw(m_spritebatch);

}

void MarkovScene::Shutdown()
{

	SaveData::SetInt("PlayerScore", PlayerScore);
	SaveData::SetInt("OppositionScore", oppositionScore);
	m_gameTimer = m_roundLength;
	PlayerScore = 0;
	oppositionScore = 0;
	delete m_pAgentTexture;
	delete m_backgroundTexture;
	//	delete m_player;
	delete m_font;
	delete m_soccerBallTextture;
	//	delete m_playerTexture;
	delete m_playerTeamTexture;

	for (int i = 0; i < numberofenemies; i++)
	{
		delete oppositionArray[i];
	}

	oppositionArray.Clear();
	for (int i = 0; i < numberofteammates; i++)
	{
		delete teamMateArray[i];
	}

	teamMateArray.Clear();

	delete m_oppositionTexture;
	delete m_ball;

}


void MarkovScene::DrawVector(Vector2 origin, Vector2 dir)
{
	m_spritebatch->DrawLine(origin.x, origin.y, origin.x + dir.x, origin.y + dir.y);
}
void MarkovScene::DrawPoint(Vector2 origin)
{
	m_spritebatch->DrawLine(origin.x - 5.0f, origin.y - 5.0f, origin.x + 5.0f, origin.y + 5.0f);
	m_spritebatch->DrawLine(origin.x + 5.0f, origin.y - 5.0f, origin.x - 5.0f, origin.y + 5.0f);
}
void MarkovScene::ResetScores()
{
	PlayerScore = 0;
	oppositionScore = 0;
}
void MarkovScene::AddScore(int iD)
{
	if (iD == 0)
	{
		PlayerScore += 1;
	}
	else if (iD == 1)
	{
		oppositionScore += 1;
	}
	MarkovScene::disableTimer = MarkovScene::disableLength;
}