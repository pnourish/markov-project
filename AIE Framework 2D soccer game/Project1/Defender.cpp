#include "Defender.h"
#include "Game1.h"
#include "Team.h"


Defender::Defender(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice) : AIAgent(a_texture, a_pos, a_teamChoice)
{
	currState = eAIState::DEFEND;
	m_fieldPosition = Character::eFIELDPOSITION::DEFENDER;
	moveSpeed = defenderSpeed;
}


Defender::~Defender()
{
}


void Defender::Update(float deltaTime)
{
	Vector2 vecBetweenOppositionBall = position - m_team->GetBall()->position;
	Vector2 vecBetweenOppositionSeekPos = position - SeekPos;
	int teamMateCount = 0;

	//switch
	switch (currState)
	{
	case eAIState::KICK:
		AIKick(deltaTime, this, m_team->GetBall());

		//do the transitions from here
		m_currKickTimer -= deltaTime;
		if (m_currKickTimer <= 0)
		{
			currState = eAIState::DEFEND;
		}

		if (vecBetweenOppositionBall.Magnitude() <= ((m_team->GetBall()->GetWidth() / 2) + (GetWidth() / 2)))
		{
			if (m_currKickTimer > 0.1f)
				m_currKickTimer = 0.1f;
		}
		break;


	case eAIState::DEFEND:
		//code for determining when to start using the avoid function
		for (int i = 0; i < m_fleePositions.Size(); i++)
		{
			Vector2 murderVec(this->position - m_fleePositions[i]->position);
			float distanceToMurder = murderVec.Magnitude();
			if (distanceToMurder <= m_avoidDistance)
			{
				AIAgent::AIAvoid(this, m_fleePositions[i]);
			}
		}
	// beggining of enter seek code
		for (int i = 0; i < this->GetTeam()->GetTeamMembersArray()->size(); i++)
		{
			if (this->GetTeam()->GetTeamMembers(i)->IsAlive() == true)
			{
				teamMateCount++;
			}
			
		}
		if (teamMateCount == 2 || teamMateCount == 1)
		{
				//AIAgent::AISeek(m_team->GetBall(), m_team->GetGoal(), this, m_windowBoundaries.x);
				currState = eAIState::SEEK;
		}
		

		AIDefend(m_team->GetBall(), m_team->GetOppositionGoal(), this, m_windowBoundaries.x);
		if (m_team->GetTeamDirection() == -1)
		{
			if (m_team->GetBall()->position.x < (m_windowBoundaries.x / 2))
			{
				AIHangBackDefence(m_team->GetOppositionGoal(), this, m_team->GetBall());
			}
			break;
		}
		else
		{
			if (m_team->GetBall()->position.x > (m_windowBoundaries.x / 2))
			{
				AIHangBackDefence(m_team->GetOppositionGoal(), this, m_team->GetBall());
			}
			break;
		}

	
	case eAIState::SEEK:
	if (teamMateCount == 3)
	{
		currState = eAIState::DEFEND;
	}
	for (int i = 0; i < m_fleePositions.Size(); i++)
	{
		Vector2 murderVec(this->position - m_fleePositions[i]->position);
		float distanceToMurder = murderVec.Magnitude();
		if (distanceToMurder <= m_avoidDistance)
		{
			AIAgent::AIAvoid(this, m_fleePositions[i]);
		}
	}
		AISeek(m_team->GetBall(), m_team->GetGoal(), this, m_windowBoundaries.x);




		if (vecBetweenOppositionSeekPos.Magnitude() <= ((m_team->GetBall()->GetWidth())) && currState == eAIState::SEEK)
		{
			m_currKickTimer = m_kickTime;
			currState = eAIState::KICK;
			kickDirection = vecBetweenOppositionBall;
		}

	

	}


	AIAgent::Update(deltaTime);
}
