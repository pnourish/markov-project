#ifndef GAME1_H
#define GAME1_H
#include "CollisionManager.h"
#include "Application.h"
#include "Player.h"
#include "Ball.h"
#include "AIAgent.h"
#include "Goal.h"
#include "MurderBot.h"
#include "StandardGameScene.h"
#include "MainMenuScene.h"
#include "GameOverScene.h"
#include "KeepingsOffScene.h"
#include "MarkovScene.h"
class SpriteBatch;

class Game1 : public Application
{
public:
	enum class eScene
	{
		MENU,
		STANDARDGAME,
		KEEPINGSOFF,
		MARKOV,
		GAMEOVER
	};
	// assets loaded in constructor
	Game1(unsigned int windowWidth, unsigned int windowHeight, bool fullscreen, const char *title);

	// assets destroyed in destructor
	virtual ~Game1();

	// update / draw called each frame automaticly
	virtual void Update(float deltaTime);
	virtual void Draw();
	



	eScene currScene;
	eScene prevScene;
	void SwitchScene(eScene nextScene);


	bool gameIsRunning = true;


protected:
	SpriteBatch *m_spritebatch;


	//keep this shit
	KeepingsOffScene keepingsOffScene;
	GameOverScene gameOverScene;
	MainMenuScene menuScene;
	StandardGameScene gameScene;
	MarkovScene markovScene;
	

	Vector2 mousePos;

	
private:

};

#endif