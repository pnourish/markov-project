#pragma once
#include <iostream>
template <typename T>
class DynamicArrayTemplate
{
public:

	DynamicArrayTemplate();
	DynamicArrayTemplate(DynamicArrayTemplate& a_other);
	DynamicArrayTemplate(int a_size);
	~DynamicArrayTemplate();


	DynamicArrayTemplate& operator = (DynamicArrayTemplate& a_other)
	{
		//dont allow self assignment
		if (&a_other == this)
		{
			return *this;
		}
		//delete any memory in m_data
		if (m_data != nullptr)
		{
			delete[] m_data;
		}
		//allocate new memory for a_other
		m_data = new int[a_other.Capacity()];
		//copy a_other data into m_data
		for (int i = 0; i < a_other.Size(); i++)
		{
			m_data[i] = a_other.m_data[i];
		}
		//return yourself
		return *this;
	}

	T& operator[](int a_index)
	{
		return m_data[a_index];
	}

	//returns the current amount that is being used by this array
	int Size();

	//returns the currount amount that is being used by a different array
	int Size(T* a_otherArray);

	//returns the current total maximum of the array
	int Capacity();

	//This changes the size of the array
	void Resize(int a_newSize);

	//This changes the size of the array to an automatic percentage of the current size
	void Resize();

	//this function creates a new array of an approrpiate size and then copies values into it
	void Set(T* a_otherArray, int len);

	//does a different thing
	void Set(DynamicArrayTemplate& a_other);

	//insert value at index (move everything after it)
	void Insert(int a_index, T a_value);

	//add value to the end of an array
	void Add(T a_value);

	//finds a_value in array and returns index
	int Find(T a_value);

	// remove first occurence of a_ value (find it first)
	void Remove(T a_value);

	//returns value at index
	int Get(T a_index);

	//swaps 2 numbers in an array
	void Swap(int a_index1, int a_index2);

	//changes an array into a sorted order via a horrible method
	void BubbleSort();

	//changes an array into a sorted order via 
	void InsertionSort();

	//This function shuffles an array a set number of times
	void Shuffle(int a_shuffles);

	// performs a search which checks every number in order to see if it is what we are looking for
	int LinearSearch( T Key);

	//does a search which works by checking a 50% number each time. array must be sorted for this to work
	int BinarySearch(T a_array, int length, T key);

	void Clear();
private:
	int m_size; // used elements
	int m_capacity; // allocated elements
	T* m_data;

};

template <typename T>
DynamicArrayTemplate<T>::DynamicArrayTemplate()
{
	//create a default array
	m_data = new T[10];
	//set all to zero
	for (int i = 0; i < 10; i++)
	{
		m_data[i] = T();
	}
	m_size = 0;
	m_capacity = 10;
}
template <typename T>
DynamicArrayTemplate<T>::DynamicArrayTemplate(int a_size)
{
	//create an array of a_size
	m_data = new int[a_size];
	// set all to zero
	for (int i = 0; i < a_size; i++)
	{
		m_data[i] = T();
	}
	m_size = 0;
	m_capacity = a_size;
}
template <typename T>
DynamicArrayTemplate<T>::~DynamicArrayTemplate()
{
	delete[] m_data;
	//delete memory
}

template <typename T>
int DynamicArrayTemplate<T>::Size()
{
	return m_size;
}
template <typename T>
int DynamicArrayTemplate<T>::Size(T* a_otherArray)
{
	return a_otherArray;
}
template <typename T>
int DynamicArrayTemplate<T>::Capacity()
{
	return m_capacity;
}
template <typename T>
void DynamicArrayTemplate<T>::Resize(int a_newSize)
{
	if (a_newSize < 1)
		a_newSize = 1;
	//alter m_data to be newSize
	T* tempArray = new T[a_newSize] { 0 };
	//copy values from m_string into tempArray
	for (int i = 0; i < Size(); i++)
	{
		tempArray[i] = m_data[i];

	}

	delete[] m_data;
	m_data = tempArray;
	m_capacity = a_newSize;
}
template <typename T>
void DynamicArrayTemplate<T>::Resize()
{
	Resize(m_size * 1.25f);
}
template <typename T>
void DynamicArrayTemplate<T>::Set(T* a_array, int len)
{
	Resize(Size(len));
	for (int i = 0; i < Size(len); i++)
	{
		m_data[i] = a_array[i];
	}
}
template <typename T>
void DynamicArrayTemplate<T>::Set(DynamicArrayTemplate& a_other)
{
	Resize(a_other.Size());
	for (int i = 0; i < Size(); i++)
	{
		m_data[i] = a_other[i];
	}
}
template <typename T>
void DynamicArrayTemplate<T>::Insert(int a_index, T a_value)
{
	if (Size() == m_capacity)
		Resize(Size());
	int currentIndex = 0;
	currentIndex = Size();
	while (currentIndex > a_index)
	{
		m_data[currentIndex] = m_data[currentIndex - 1];
		currentIndex--;
	}
	m_data[currentIndex] = a_value;
	m_size++;
}
template <typename T>
void DynamicArrayTemplate<T>::Add(T a_value)
{
	Insert(Size(), a_value);
}

template <typename T>
int DynamicArrayTemplate<T>::Find(T a_value)
{
	bool wasFound = true;
	int currentPoint;
	while (wasFound = true)
	{
		if (m_data[currentPoint] == a_value)
		{
			return currentPoint;
		}
		else
		{
			currentPoint++;
		}
		if (currentPoint == m_capacity)
		{
			return -1;
		}
	}
}
template <typename T>
void DynamicArrayTemplate<T>::Remove(T a_value)
{
	int currentIndex = a_value;
	while (currentIndex < Size())
	{
		m_data[currentIndex] = m_data[currentIndex + 1];
		currentIndex++;
	}
	if (m_size < (m_capacity *0.5f))
	{
		Resize(m_capacity * 0.75f);
	}
}
template <typename T>
int DynamicArrayTemplate<T>::Get(T a_index)
{
	return m_data[a_index];
}
template <typename T>
void  DynamicArrayTemplate<T>::Swap(int a_index1, int a_index2)
{
	T temp = 0;
	temp = m_data[a_index1];
	m_data[a_index1] = m_data[a_index2];
	m_data[a_index2] = temp;
}
template <typename T>
void DynamicArrayTemplate<T>::BubbleSort()
{
	//soirting goes here
	bool sorted = false;
	//while not sorted
	while (sorted == false)
	{
		//assume we're sorted
		sorted = true;
		for (int i = 0; i < m_size-1; i++)
		{
			if (m_data[i] > m_data[i + 1])
			{
				//we might not be sorted anymore
				Swap(i, i + 1);
					sorted = false;
			}
		}
	}
}
template <typename T>
void DynamicArrayTemplate<T>::InsertionSort()
{
	for (int i = 2; i < Size(); i++)
	{
		int key = m_data[i];
		int j = i - 1;
		while (j > 0 && m_data[j] > key)
		{
			m_data[j + 1] = m_data[j];
			j = j - 1;
			m_data[j + 1] = key;
		}
	}
}
template <typename T>
void DynamicArrayTemplate<T>::Shuffle(int a_shuffles)
{
	for (int i = 0; i < a_shuffles; i++)
	{
		Swap(rand() % size, rand() % size);
	}
}
template <typename T>
int DynamicArrayTemplate<T>::LinearSearch( T Key)
{
	for (int i = 0; i < Size(); i++)
	{
		if (m_data[i] == Key)
		{
			return m_data[i];
		}
	}
	return -1;
}
template <typename T>
int DynamicArrayTemplate<T>::BinarySearch(T a_array, int length, T toFind)
{
	int maxLength = length - 1;
	int minLength = 0;
	while (maxLength != minLength)
	{
		int middle = (minLength + maxLength) / 2;
		if (a_array[middle] == toFind)
		{
			return a_array[middle];
		}
		else if (a_array[middle] > toFind)
		{
			maxLength = a_array[middle] - 1;
		}
		else if (a_array[middle] > toFind)
		{
			maxLength = a_array[middle] + 1;
		}
	}
	return -1
}

template<typename T>
inline void DynamicArrayTemplate<T>::Clear()
{
	m_size = 0;
}
