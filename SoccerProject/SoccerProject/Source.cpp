#include <iostream>
#include <random>
#include <ctime>
#include <set>
#include <assert.h>

#include "markov.h"

int shotDirection = 0;
int goalkeeperSaveDirection = 0;
int m_previousShot = 0;
int m_secondPreviousShot = 0;

int Shotchoice()
{
		std::cin >> shotDirection;
		switch (shotDirection)
		{

		case 1:
		{
			std::cout << "you shot towards the Top left corner of the goal, and the keeper went..." << std::endl;
			return 1;
			break;
		};
		case 2:
		{
			//shoot in this direction
			std::cout << "you shot towards the Top middle of the goal, and the keeper went..." << std::endl;
			return 2;
			break;
		};
		case 3:
		{
			//shoot in this direction
			std::cout << "you shot towards the Top right corner of the goal, and the keeper went..." << std::endl;
			return 3;
			break;
		};
		case 4:
		{
			//shoot in this direction
			std::cout << "you shot towards the Bottom left corner of the goal, and the keeper went..." << std::endl;
			return 4;
			break;
		};
		case 5:
		{
			//shoot in this direction
			std::cout << "you shot towards the Bottom middle of the goal, and the keeper went..." << std::endl;
			return 5;
			break;
		};
		case 6:
		{
			//shoot in this direction
			std::cout << "you shot towards the Bottom right corner of the goal, and the keeper went..." << std::endl;
			return 6;
			break;
			
		};

		default:
			std::cout << "The pressure of the moment cause you to stutter, try to choose again" << std::endl;
			break;
		}
}
int GoalkeeperSaveDirection(int a)
{
	//srand(time(NULL));
	goalkeeperSaveDirection = a;
	

	switch (goalkeeperSaveDirection)
	{

	case 1:
	{
		std::cout << "The goalkeeper dived toward the top left corner of the goal" << std::endl;
		return 1;
		break;
	};
	case 2:
	{
		//shoot in this direction
		std::cout << "The goalkeeper jumped up into the middle of the goal" << std::endl;
		return 2;
		break;
	};
	case 3:
	{
		//shoot in this direction
		std::cout << "The goalkeeper dived toward the top right corner of the goal" << std::endl;
		return 3; 
		break;
	};
	case 4:
	{
		//shoot in this direction
		std::cout << "The goalkeeper dived toward the bottom left corner of the goal" << std::endl;
		return 4; 
		break;
	};
	case 5:
	{
		//shoot in this direction
		std::cout << "The goalkeeper stayed still in the middle goal" << std::endl;
		return 5; 
		break;
	};
	case 6:
	{
		//shoot in this direction
		std::cout << "The goalkeeper dived toward the bottom right corner of the goal" << std::endl;
		return 6; 
		break;
	};

	default:
		std::cout << "The goalkeeper didnt move" << std::endl;
		return 5;
		break;
	}
}
void ShotResult(int playerShot, int goalieChoice)
{

	if (shotDirection == goalkeeperSaveDirection)
	{
		std::cout << "The goalkeeper saved your shot.\nYou let down your team. your contract will not be renewed." << std::endl;
	}
	else
	{
		std::cout << "Your shot made it past the keeper.\nYou're the greatest player of all time, good for you" << std::endl;

	}
}
int main()
{

	char a  = 1;
	char b  = 2;
	char c  = 3;
	char d  = 4;

	markov<int> test;


	test.GetNext(a, b);




	bool repeatGame = true;
	char playAgain[100] = { 0 };
	
	//decision of where the player shoots
	while (repeatGame == true)
	{


		std::cout << "you have to take a penalty shot against a goal keeper \nPlease choose where you would like to shoot\n\n\n" << std::endl;
		std::cout << "Top left = 1\nTop middle = 2\nTop right = 3\nBottom left = 4\nBottom middle = 5\nBottom right = 6\n " << std::endl;


		Shotchoice();
		GoalkeeperSaveDirection(test.GetNext(m_previousShot, m_secondPreviousShot));
		ShotResult(shotDirection, goalkeeperSaveDirection);
		test.Add(m_secondPreviousShot, m_previousShot, shotDirection);
		m_secondPreviousShot = m_previousShot;
		m_previousShot = shotDirection;



		std::cout << "Press Y if you would like to play again and N if you would like to stop." << std::endl;
		std::cin >> playAgain;
		

	}
	
























	system("pause");


}