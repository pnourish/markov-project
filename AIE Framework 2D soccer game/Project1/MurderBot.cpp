#include "MurderBot.h"



MurderBot::MurderBot(Texture* a_texture, Vector2 a_pos, Team* a_teamChoice) : AIAgent(a_texture, a_pos, a_teamChoice)
{
	currState = eAIState::MURDER;
	m_fieldPosition = Character::eFIELDPOSITION::MURDERBOT;
	moveSpeed = murderBotSpeed;
}


MurderBot::~MurderBot()
{
}



void MurderBot::Update(float deltaTime)
{
	AIPatrol(this, m_windowBoundaries.x, m_windowBoundaries.y);

	AIAgent::Update(deltaTime);
}
