#pragma once
#include "Vector2.h"
#include <vector>

class SoccerDebugDraw
{
	enum DrawTypes
	{
		POINT,
		LINE,
		SPHERE
	};

	struct DrawInfo
	{
		DrawTypes eType;
	};

	struct PointDrawInfo : public DrawInfo
	{
		Vector2 Position;
		unsigned char r, g, b, a;
	};

public:
	~SoccerDebugDraw();
	static SoccerDebugDraw* GetInstance();
	static void				DestroySingleton();

	void Clear();
	void Draw(class SpriteBatch* pSpriteBatch);

	void AddDrawPoint(const Vector2& Position, unsigned char r, unsigned char g, unsigned char b, unsigned char a);
private:
	SoccerDebugDraw();
	void DrawPoint(PointDrawInfo* pPointInfo, class SpriteBatch* pSpriteBatch);


	static SoccerDebugDraw* sm_instance;

	std::vector<DrawInfo*> m_drawList;
};