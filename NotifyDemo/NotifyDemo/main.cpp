#include <iostream>
#include <vector>

const int EVENT_KICK = 1;
const int EVENT_RECEIVE = 2;
const int EVENT_STOP = 3;

class IObserver
{
public:
	virtual void OnNotify(int EventID, void* data) = 0;
};

class ISubject
{
public:
	void AddObserver(IObserver* observer) { m_observers.push_back(observer); }
	void Notify(int EventID, void* data)
	{
		for (auto& observer : m_observers)
		{
			observer->OnNotify(EventID, data);
		}
	}
private:
	std::vector<IObserver*> m_observers;
};



class Markov : public IObserver
{
	void DoMarkovCalculation()
	{

	}

	void OnBallKicked()
	{
		//Update data

	}

	void OnNotify(int EventID, void* data)
	{
		switch (EventID)
		{
		case EVENT_KICK:
			std::cout << "They kicked the ball!" << std::endl;
			OnBallKicked();
			break;
		default:
			break;
		}
	}
};

class Ball : public ISubject
{
public:
	void DoKick()
	{
		Notify(EVENT_KICK, nullptr);
	}
};

class Player
{
public:
	Ball* pBall;

	void Update()
	{
		pBall->DoKick();
	}
};

class AI : IObserver
{
public:
	void Update();

	Markov* pMarkov;
};

void main()
{
	Ball* pBall = new Ball();

	Player* pPlayer = new Player();
	pPlayer->pBall = pBall;

	Markov* pMarkov = new Markov();
	pBall->AddObserver(pMarkov);
	pBall->AddObserver(pAI);

	pPlayer->Update();
}