#include "Button.h"



Button::Button()
{
}

Button::Button(Vector2 a_position, float a_height, float a_width, Font* a_textFont)
{
	m_position = a_position;
	m_height = a_height;
	m_width = a_width;
	m_textFont = a_textFont;
}


Button::~Button()
{
}
//do nothing in this, make application determine whether the button has been pressed or not
void Button::Update(float deltaTime)
{
	
}

void Button::Draw(SpriteBatch* m_spritebatch)
{
	

	m_spritebatch->DrawLine(m_position.x, m_position.y, m_position.x + m_width, m_position.y, 2);
	m_spritebatch->DrawLine(m_position.x + m_width, m_position.y, m_position.x + m_width, m_position.y + m_height, 2);
	m_spritebatch->DrawLine(m_position.x + m_width, m_position.y + m_height, m_position.x , m_position.y + m_height, 2);
	m_spritebatch->DrawLine(m_position.x, m_position.y + m_height, m_position.x, m_position.y, 2);
			
	m_spritebatch->DrawString(m_textFont, m_message.c_str(), m_position.x + 10, m_position.y );



}

bool Button::checkPos(Vector2 a_objectToCheck)
{
	if ((a_objectToCheck.x > m_position.x && a_objectToCheck.x < m_position.x + m_width) && (a_objectToCheck.y > m_position.y && a_objectToCheck.y < m_position.y + m_height))
	{
		return true;
	}
	else
	{
	return false;

	}
}

void Button::setMessage(std::string messageToAdd)
{
	m_message = messageToAdd;
}
