#include "FlyCamera.h"
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>


FlyCamera::FlyCamera(GLFWwindow* window, float speed)
	: m_speed(speed)
	, m_window(window)
{
}




void FlyCamera::Update(float deltaTime)
{
	glm::mat4 transform = GetWorldTransform();
	glm::vec4 vRight = transform[0];
	glm::vec4 vUp = transform[1];
	glm::vec4 vForward = transform[2];

	glm::vec4 moveDirection;

	if (glfwGetKey(m_window, GLFW_KEY_W))
	{
		moveDirection -= (vForward * m_speed);
	}

	if (glfwGetKey(m_window, GLFW_KEY_S))
	{
		moveDirection += (vForward * m_speed);
	}

	if (glfwGetKey(m_window, GLFW_KEY_D))
	{
		moveDirection += (vRight * m_speed);
	}

	if (glfwGetKey(m_window, GLFW_KEY_A))
	{
		moveDirection -= (vRight * m_speed);
	}

	if (glfwGetKey(m_window, GLFW_KEY_E))
	{
		moveDirection += (vUp * m_speed);
	}

	if (glfwGetKey(m_window, GLFW_KEY_Q))
	{
		moveDirection -= (vUp * m_speed);
	}

	if (glm::length(moveDirection) > 0.0f)
	{
		transform[3] += moveDirection * deltaTime;
		setTransform(transform);
	}
	BasicRotation();

}

void FlyCamera::SetSpeed(float a_speed)
{
	m_speed = a_speed;
}

void FlyCamera::BasicRotation()
{
	glm::mat4 rot(1);
	if (glfwGetKey(m_window, GLFW_KEY_RIGHT))
	{
		rot = glm::rotate(m_speed * 0.005f , glm::vec3(0, -1, 0));
		setTransform(GetWorldTransform()* rot);
	}

	if (glfwGetKey(m_window, GLFW_KEY_LEFT))
	{
		rot = glm::rotate(m_speed * 0.005f, glm::vec3(0, 1, 0));
		setTransform(GetWorldTransform()* rot);
	}

	if (glfwGetKey(m_window, GLFW_KEY_UP))
	{
		rot = glm::rotate(m_speed * 0.005f, glm::vec3(1, 0, 0));
		setTransform(GetWorldTransform()* rot);
	}

	if (glfwGetKey(m_window, GLFW_KEY_DOWN))
	{
		rot = glm::rotate(m_speed * 0.005f, glm::vec3(-1, 0, 0));
		setTransform(GetWorldTransform()* rot);
	}
}


