#include "Entity.h"

Entity::Entity()
	: m_entityTexture(nullptr)
{
}

Entity::Entity(Texture* a_texture, Vector2 a_pos)
{
	m_entityTexture = a_texture;
	position = a_pos;
}

Entity::~Entity()
{
}

void Entity::Draw(SpriteBatch* a_spriteBatch)
{
	a_spriteBatch->DrawSprite(m_entityTexture, position.x, position.y);
}
void Entity::SetVelocity(Vector2 a_velocity)
{
	m_velocity = a_velocity;
}
Vector2 Entity::GetVelocity()
{
	return m_velocity;
}
void Entity::Drag()
{
	float dragSpeed = 0.991f;
	m_velocity = m_velocity * dragSpeed;
}
void Entity::SetWindowBoundaries(Vector2 a_boundaries)
{
	m_windowBoundaries = a_boundaries;
}
void Entity::SetSpawn(Vector2 spawnPos)
{
	m_spawnPos = spawnPos;
}
Vector2 Entity::GetSpawn()
{
	return m_spawnPos;
}
void Entity::Respawn()
{
	
	SetAlive(true);
	position = m_spawnPos;
	m_velocity = Vector2();
}
float Entity::GetWidth()
{
	return m_entityTexture->GetWidth();
}
float Entity::GetHeight()
{
	return m_entityTexture->GetHeight();
}
void Entity::SetTeam(Team* a_team)
{
	m_team = a_team;
}
Team* Entity::GetTeam()
{
	return m_team;
}

void Entity::SetAlive(bool a_lifeState)
{
	m_isAlive = a_lifeState;
}

void Entity::SetDead()
{
	m_isAlive = false;
}

bool Entity::IsAlive()
{
	if (m_isAlive == true)
	{
		return true;
	}
	else return false;
}
