#include <map>
#include <vector>
#include <random>
#include <string>
#include <unordered_map>

using StringKey = std::pair<std::string, std::string>;
using IntKey = std::pair<int, int>;
using CharKey = std::pair<char, char>;

namespace std
{
	template<>
	struct hash<StringKey>
	{
		std::size_t operator()(const StringKey& k) const
		{
			std::string concate = k.first + k.second;
			return std::hash<std::string>{}(concate);
		}
	};
	template<>
	struct hash<IntKey>
	{
		std::size_t operator()(const IntKey& k) const
		{

			int k1 = k.first;
			int k2 = k.second;

			int hash = (int)(0.5f * (k1 + k2) * (k1 + k2 + 1) + k2);

			return hash;
		}
	};
	template<>
	struct hash<CharKey>
	{
		std::size_t operator()(const CharKey& k)const
		{
			char k1 = k.first;
			char k2 = k.second;

			int hash = (int)(0.5f * (k1 + k2) * (k1 + k2 + 1) + k2);

			return hash;
		}
	};
}



template<typename T>
class markov
{
public:
	markov() : m_rdevice()
		, m_generator(m_rdevice())
	{

	}
	~markov()
	{

	}

private:
	using Combo = std::pair<T, T>;
	using History = std::unordered_map<T, int>;

public:


	void Add(T oldest, T middle, T actualShot)
	{
		//int hash = m_hasher.GetHash(firstPrevious, secondPrevious);
		History& history = m_history[Combo(oldest, middle)];
		history[actualShot] += 1;
	}

	T GetNext(T oldest, T middle)
	{
		History& history = m_history[Combo(oldest, middle)];
		//int selected = RunRouletteWheel(history);
		int selected = PickBest(history);
		return selected;
	}

	T PickBest(History& a_choiceHistory)
	{
	
		int largest = 0;
		const	T* currentBest = nullptr;
		for (auto it = a_choiceHistory.begin(); it != a_choiceHistory.end(); ++it)
		{
			if (it->second > largest)
			{
				largest = it->second;
				currentBest = &(it->first);
			}
		}
		if (currentBest == nullptr) return -1;
		else return *currentBest;
	}

	T RunRouletteWheel(History& a_choiceHistory)
	{
		//Get total shot count
		//set attemptCount to sum of all attemps in history
		//set rValue to random value from 0 to attemptCount
		//iterate through shot history
		//if attempt value is less than rValue
		// return attempt value
		//else
		// set rValue to rValue - attempt value

		int sum = 0;
		for (auto it = a_choiceHistory.begin(); it != a_choiceHistory.end(); ++it)
		{
			sum += it->second;
		}

		if (sum == 0)
		{
			std::uniform_int_distribution<int> dist(1, 6);

			return dist(m_generator);
		}
		//here we set the random value for rValue


		std::uniform_int_distribution<int> dist(0, sum -1);

		int rValue = dist(m_generator);
		for (auto it = a_choiceHistory.begin(); it != a_choiceHistory.end(); ++it)
		{
			if (it->second >= rValue)
			{
				return it->first;
			}
			else
			{
				sum -= rValue;
			}
		}
		return -1;
	}

	void FillPreviousHistory(int a_numberOfPreviousShots)
	{
		std::uniform_int_distribution<int> dist(1, 6);



		//create for loop here to fill the previous data
		for (int i = 0; i < a_numberOfPreviousShots; i++)
		{
			int oldestChoice = dist(m_generator);
			int middleChoice = dist(m_generator);
			int nextChoice = dist(m_generator);
			Add(oldestChoice, middleChoice, nextChoice);
		}
	}

private:
	

	std::unordered_map<Combo, History> m_history;

	std::random_device m_rdevice;
	std::mt19937 m_generator;
};


