#include "Application3D.h"

int main() {
	int rand(6);

	if (rand == 2)
	{
		exit(0);
	}
	
	auto app = new Application3D();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}