#include "BaseCamera.h"



BaseCamera::BaseCamera()
	:m_worldTransform(1)
	, m_projectionTransform(1)
{
	UpdateViewFromWorld();
}


BaseCamera::~BaseCamera()
{
}


//void BaseCamera::Update(float deltaTime)
//{
//}



void BaseCamera::setLookAt(glm::vec3 from, glm::vec3 to, glm::vec3 up)
{
}

void BaseCamera::SetPosition(const glm::vec3 position)
{
	m_worldTransform[3] = glm::vec4(position, 1);
	UpdateViewFromWorld();
}

void BaseCamera::lookAt(glm::vec3 eye, glm::vec3 centre, glm::vec3 worldUp)
{
	m_viewTransform = glm::lookAt(eye, centre, worldUp);
	UpdateWorldFromView();
}

void BaseCamera::setPerspective(float vFOV, float aspectRation, float nearDistance, float farDistance)
{
	m_projectionTransform = glm::perspective(vFOV, aspectRation, nearDistance, farDistance);

}

glm::mat4 const BaseCamera::GetWorldTransform()
{
	return m_worldTransform;
}

glm::mat4 const BaseCamera::GetView()
{
	return m_viewTransform;
}

glm::mat4 const BaseCamera::GetProjection()
{
	return m_projectionTransform;
}

glm::mat4 const BaseCamera::GetProjectionView()
{
	return m_projectionTransform * m_viewTransform;
}


void BaseCamera::UpdateViewFromWorld()
{
	m_viewTransform = glm::inverse(m_worldTransform);
}

void BaseCamera::UpdateWorldFromView()
{
	m_worldTransform = glm::inverse(m_viewTransform);
}

void BaseCamera::UpdateProjectionViewTransform()
{
}
