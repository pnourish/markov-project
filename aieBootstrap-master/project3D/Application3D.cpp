#include "Application3D.h"
#include "FlyCamera.h"




Application3D::Application3D() {
}

Application3D::~Application3D() {

}

bool Application3D::startup() {
	
	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(64000, 64000, 64000, 64000);

	// create simple camera transforms
	//m_viewMatrix = glm::lookAt(vec3(20), vec3(0), vec3(5, 10, 5));
	//m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
		//								  getWindowWidth() / (float)getWindowHeight(),
			//							  0.1f, 1000.f);
//	m_orthographicMatrix = glm::ortho(-1, 1, -1, 1, 2, 4);

	m_camera = new FlyCamera(m_window, 5.0f);
	m_camera->setPerspective(glm::pi<float>() * 0.25f, getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.f);
	m_camera->lookAt(glm::vec3(20, 20, 20), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));


	setupShader();
	generateGrid(150, 150);
	


	return true;
}

void Application3D::shutdown() {

	Gizmos::destroy();
}

void Application3D::update(float deltaTime) {

	// query time since application started
	float time = getTime();

	// rotate camera
	//m_viewMatrix = glm::lookAt(vec3(glm::sin(time) * 10, 10, glm::cos(time) * 10),
	//						   vec3(0), vec3(0, 1, 0));

	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// draw a simple grid with gizmos
//	vec4 white(1);
//	vec4 black(0, 0, 0, 1);
//	for (int i = 0; i < 21; ++i) {
//		Gizmos::addLine(vec3(-10 + i, 0, 10),
//						vec3(-10 + i, 0, -10),
//						i == 10 ? white : black);
//		Gizmos::addLine(vec3(10, 0, -10 + i),
//						vec3(-10, 0, -10 + i),
//						i == 10 ? white : black);
//	}

	// add a transform so that we can see the axis
	//Gizmos::addTransform(mat4(1));

	// demonstrate a few shapes
	//Gizmos::addAABBFilled(vec3(0), vec3(1), vec4(0, 0.5f, 1, 0.25f));
	//Gizmos::addSphere(vec3(5, 0, 5), 1, 8, 8, vec4(1, 0, 0, 0.5f));
	//Gizmos::addRing(vec3(5, 0, -5), 1, 1.5f, 8, vec4(0, 1, 0, 1));
	//Gizmos::addDisk(vec3(-5, 0, 5), 1, 16, vec4(1, 1, 0, 1));
	//Gizmos::addArc(vec3(-5, 0, -5), 0, 2, 1, 8, vec4(1, 0, 1, 1));

	//mat4 t = glm::rotate(time, glm::normalize(vec3(1, 1, 1)));
	//t[3] = vec4(-2, 0, 0, 1);
	//Gizmos::addCylinderFilled(vec3(0), 0.5f, 1, 5, vec4(0, 1, 1, 1), &t);





	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	m_camera->Update(deltaTime);

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void Application3D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// update perspective in case window resized
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f,
										  getWindowWidth() / (float)getWindowHeight(),
										  0.1f, 1000.f);

	glm::mat4 projView = m_camera->GetProjectionView();;


	glUseProgram(m_programID);
	unsigned int projectionViewGlobalLocation = glGetUniformLocation(m_programID, "projectionViewGlobalMatrix");
	assert(projectionViewGlobalLocation != -1);
	glUniformMatrix4fv(projectionViewGlobalLocation, 1, false, glm::value_ptr(projView));

	unsigned int timeLocation = glGetUniformLocation(m_programID, "time");
	glUniform1f(timeLocation, getTime());

	glBindVertexArray(m_VAO);
	glDrawElements(GL_TRIANGLES, m_numberofIndices, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


	//Gizmos::draw(m_camera->GetProjectionView());
	//Gizmos::draw(m_projectionMatrix * m_viewMatrix);
}

void Application3D::generateGrid(unsigned int rows, unsigned int cols)
{


	//allocate some memory to hold vertex data
	Vertex* aoVertices = new Vertex[rows * cols];
	
	unsigned int numberOfIndices = (rows - 1) * (cols - 1) * 6;
	unsigned int* auiIndices = new unsigned int[(rows - 1) * (cols - 1) * 6];

	// populate memory with vertex data
	for (unsigned int r = 0; r < rows; r++)
	{
		for (unsigned int c = 0; c < cols; c++)
		{
			Vertex& vert = aoVertices[r*cols + c];
			vert.position = vec4((float)c, 0, (float)r, 1);
			//create some arbitrary colour based off something
			//that might not be related to tiling a texture
			vert.colour = vec4(sinf((c / (float)(cols - 1)) * (r / (float)(rows - 1))));
			//this line sets the colour to red
			vert.colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);

		}
	}

	//defining index count based off quad count (2 triangles per quad)

	unsigned int index = 0;
	for (unsigned int r = 0; r < (rows - 1); ++r) 
	{
		for (unsigned int c = 0; c < (cols - 1); ++c)
		{
			//triangle 1
			auiIndices[index++] = r*cols + c;
			auiIndices[index++] = (r + 1) * cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			//traingle 2
			auiIndices[index++] = r* cols + c;
			auiIndices[index++] = (r + 1) * cols + (c + 1);
			auiIndices[index++] = r * cols + (c + 1);
		}
	}

	glGenBuffers(1, &m_IBO);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (rows - 1) * (cols - 1) * 6 * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	//
	//send data to the graphics card
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, (rows * cols) * sizeof(Vertex), aoVertices, GL_STATIC_DRAW);

	

	glBindBuffer(GL_ARRAY_BUFFER, 0);




	glGenBuffers(1, &m_IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numberOfIndices * sizeof(unsigned int), auiIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//generate vertex array object
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IBO);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(vec4)));

	glBindVertexArray(0);




	//delete vertices
	delete[] aoVertices;
	delete[] auiIndices;


	m_numberofIndices = numberOfIndices;
}

void Application3D::setupShader()
{

	//create shaders
	const char* vsSource = "#version 410\n\n \
					 layout(location=0) in vec4 position; \
					 layout(location=1) in vec4 colour; \
					 out vec4 vColour; \
                     out vec4 warpedPosition;\
					 uniform float time;  \
					 uniform mat4 projectionViewGlobalMatrix; \
					 void main() \
					 {\
						vColour = colour; \
						warpedPosition = position; \
						warpedPosition.y = sin(time + position.x); \
						warpedPosition.y -= cos(time + position.z); \
						gl_Position = projectionViewGlobalMatrix * warpedPosition; \
				     }\
					";

	const char* fsSource = "#version 410\n\n \
					 in vec4 vColour; \
					 out vec4 fragColor; \
					 in vec4 warpedPosition; \
					 void main() \
					{\
							fragColor = vec4(warpedPosition.yyy, 1); \
					}\
				";

/*
						if(warpedPosition.y > .01f)\
						{\
							fragColor = warpedPosition ; \
						}\
						else \
						{ fragColor = vec4(0,0,1,1); \
						}\
					}\
	*/

		



	int success = GL_FALSE;
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, (const char**)&vsSource, 0);
	glCompileShader(vertexShader);

	glShaderSource(fragmentShader, 1, (const char**)&fsSource, 0);
	glCompileShader(fragmentShader);

	m_programID = glCreateProgram();
	glAttachShader(m_programID, vertexShader);
	glAttachShader(m_programID, fragmentShader);
	glLinkProgram(m_programID);

	glGetProgramiv(m_programID, GL_LINK_STATUS, &success);
	if (success == GL_FALSE)
	{
		int infoLogLength = 0;
		glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];

		glGetProgramInfoLog(m_programID, infoLogLength, 0, infoLog);
		printf("Error: failed to link shader program!\n");
		printf("%s\n", infoLog);
		delete[] infoLog;

	}
	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);
}
