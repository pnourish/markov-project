#pragma once
#include "CollisionManager.h"
#include "Player.h"
#include "Ball.h"
#include "AIAgent.h"
#include "Goal.h"
#include "MurderBot.h"
class StandardGameScene
{
public:
	StandardGameScene();
	~StandardGameScene();

	void Startup();//allocate all resources, create ageents, 
	//create player, create everything
	void Update(float deltaTime);
	void DeltaTimeUpdate(float deltaTime);

	void Draw(SpriteBatch* m_spritebatch);

	void Shutdown(); //destroy everything

	bool hasGameEnded = false;


	void DrawVector(Vector2 origin, Vector2, SpriteBatch* m_spritebatch);
	void DrawPoint(Vector2 origin, SpriteBatch* m_spritebatch);
	void ScoreRestart();
	void ResetScores();
	static void AddScore(int teamid);
	static Vector2 vecFromGoal;
	static Vector2 SeekPos;
	CollisionManager collisionManager;


	int windowWidth = 1232;
	int windowHeight = 878;

	//vector of entities
	//textures (maybe application holds textures


protected:
	//SpriteBatch *m_spritebatch;


private:
	Texture *m_backgroundTexture;
	Texture *m_pAgentTexture;
	Texture *m_soccerBallTextture;
	Texture *m_playerTexture;
	Texture *m_playerTeamTexture;
	Texture *m_oppositionTexture;
	Texture *m_playerGoalTexture;
	Texture *m_oppositionGoalTexture;
	Texture *m_murderBotTexture;
	float m_gameTimer = 180;
	float m_roundLength = 180;
	static int PlayerScore;
	static int oppositionScore;
	static float disableTimer;
	static float disableLength;
	Font *m_font;
	int m_mouseX;
	int m_mouseY;
	Player* m_player;
	Ball* m_ball;
	DynamicArrayTemplate<AIAgent*> oppositionArray;
	int numberofenemies = 3;
	DynamicArrayTemplate<AIAgent*> teamMateArray;
	DynamicArrayTemplate<MurderBot*> MurderBotArray;
	int numberOfMurderBots = 2;
	int numberofteammates = 2;
	Goal* m_playerGoal;
	Goal* m_oppositionGoal;
	Input* m_input;
	float m_DeltaTime;
	Vector2 startValue;
	Vector2 endValue;
	Vector2 noVelocity;
	Team team1;
	Team team2;

	


};

