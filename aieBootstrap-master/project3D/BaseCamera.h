#pragma once
#include <glm\glm.hpp>
#include <glm/mat4x4.hpp>
#include <glm/ext.hpp>
#include <gl_core_4_4.h>
#include <GLFW\glfw3.h>
#include <glm\gtc\matrix_transform.hpp>


class BaseCamera
{
public:
	BaseCamera();
	~BaseCamera();

	virtual void Update(float deltaTime) {}
	void setLookAt(glm::vec3 from, glm::vec3 to, glm::vec3 up);
	void SetPosition(const glm::vec3 position);

	void lookAt(glm::vec3 eye, glm::vec3 centre, glm::vec3 worldUp);

	void setPerspective(float vFOV, float aspectRation, float nearDistance, float farDistance);
	
	glm::vec3 GetPosition()const { return glm::vec3(m_worldTransform[3].x, m_worldTransform[3].y, m_worldTransform[3].z); };
	
	void setTransform(const glm::mat4 trans) { m_worldTransform = trans; UpdateViewFromWorld(); }
	glm::mat4 const GetWorldTransform();
	glm::mat4 const GetView();
	glm::mat4 const GetProjection();
	glm::mat4 const GetProjectionView();


private:
	glm::mat4 m_worldTransform;
	glm::mat4 m_viewTransform;
	glm::mat4 m_projectionTransform;
	//glm::mat4 projectionViewTransform;


	void UpdateViewFromWorld();
	void UpdateWorldFromView();



	void UpdateProjectionViewTransform();
};

