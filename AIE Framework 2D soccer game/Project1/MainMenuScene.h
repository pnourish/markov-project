#pragma once
#include "Button.h"
#include "Input.h"

class MainMenuScene
{
public:
	MainMenuScene();
	~MainMenuScene();

	void Startup();//allocate all resources, create ageents, 
				   //create player, create everything
	void Update();
	void Draw(SpriteBatch* m_spritebatch);

	void Shutdown(); //destroy everything

					 //vector of entities
					 //textures (maybe application holds textures

	int windowWidth = 1232;
	int windowHeight = 878;

	Button* standardGame;
	Button* KeepingsOff;
	Button* Markov;
	Button* Quit;

protected:
	SpriteBatch *m_spritebatch;
private:
	Font *m_font;


};

